package ru.alwyd.finalproject.services;

import ru.alwyd.finalproject.entity.Doctor;
import ru.alwyd.finalproject.entity.User;

import java.util.List;

public interface DoctorService {
    void save(Doctor doctor);
    List<Doctor> findBySpeciality(String speciality);
    List<Doctor> findAll();
    Doctor findByUserEmailAddress(String email);
    int findCount();
    Doctor findByUserId(Long id);
    void addDoctor(User user, String speciality);
}
