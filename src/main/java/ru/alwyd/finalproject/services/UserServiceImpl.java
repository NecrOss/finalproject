package ru.alwyd.finalproject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alwyd.finalproject.entity.User;
import ru.alwyd.finalproject.repositories.UserDAO;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User save(User user) {
        return userDAO.save(user);
    }

    @Override
    public User update(User user) {
        userDAO.update(user);
        return user;
    }

    @Override
    public User doesUserExist(String email) {
        List<User> users = (List<User>) userDAO.findByEmail(email);
        if (users.size() == 0)
            return null;
        return users.get(0);
    }

    @Override
    public User getByEmail(String email) {
        return this.doesUserExist(email);
    }

    @Override
    public User isValidUser(String email, String password) {
        List<User> users = userDAO.findByEmailAndPassword(email, password);
        return users.get(0);
    }
}
