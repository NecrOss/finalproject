package ru.alwyd.finalproject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alwyd.finalproject.entity.Rx;
import ru.alwyd.finalproject.repositories.RxDAO;

import java.util.List;

@Service
public class RxServiceImpl implements RxService {

    @Autowired
    private RxDAO rxDAO;

    @Override
    public void save(Rx rx) {
        rxDAO.save(rx);
    }

    @Override
    public List<Rx> findByDoctorId(Long id) {
        return rxDAO.findByDoctorId(id);
    }

    @Override
    public List<Rx> findByPatientId(Long id) {
        return rxDAO.findByUserId(id);
    }
}
