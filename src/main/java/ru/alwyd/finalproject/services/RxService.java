package ru.alwyd.finalproject.services;

import ru.alwyd.finalproject.entity.Rx;

import java.util.List;

public interface RxService {
    void save(Rx rx);
    List<Rx> findByDoctorId(Long id);
    List<Rx> findByPatientId(Long id);
}
