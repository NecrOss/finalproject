package ru.alwyd.finalproject.services;

import ru.alwyd.finalproject.entity.User;

public interface UserService {
    User save(User user);
    User update(User user);
    User doesUserExist(String name);
    User getByEmail(String email);
    User isValidUser(String name, String email);
}
