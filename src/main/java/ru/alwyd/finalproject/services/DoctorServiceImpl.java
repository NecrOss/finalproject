package ru.alwyd.finalproject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alwyd.finalproject.entity.Doctor;
import ru.alwyd.finalproject.entity.User;
import ru.alwyd.finalproject.repositories.DoctorDAO;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorDAO doctorDAO;

    @Autowired
    private UserService userService;

    @Override
    public void save(Doctor doctor) {
        doctorDAO.save(doctor);
    }

    @Override
    public List<Doctor> findBySpeciality(String specialityCode) {
        return doctorDAO.findBySpecialityCode(specialityCode);
    }

    @Override
    public List<Doctor> findAll() {
        return doctorDAO.findAll();
    }

    @Override
    public Doctor findByUserEmailAddress(String email) {
        User user = userService.getByEmail(email);

        return this.findByUserId(user.getId());
    }

    @Override
    public int findCount() {
        return doctorDAO.findAllCount();
    }

    @Override
    public Doctor findByUserId(Long id) {
        return doctorDAO.findByUserId(id);
    }

    @Override
    public void addDoctor(User user, String speciality) {
        Doctor doctor = new Doctor();
        doctor.setCreateTime(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        doctor.setSpecialityCode(speciality);
        doctor.setUserId(user.getId());
        save(doctor);
    }
}
