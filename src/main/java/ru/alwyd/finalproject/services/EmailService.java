package ru.alwyd.finalproject.services;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
