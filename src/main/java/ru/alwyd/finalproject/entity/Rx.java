package ru.alwyd.finalproject.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
public class Rx {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private String symptoms;
    private String medicine;

    @Column(name = "create_time")
    private Timestamp createTime;

    @Column(name = "last_updated")
    private Timestamp lastUpdated;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "doctor_id")
    private Long doctorId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    private Doctor doctor;

}
