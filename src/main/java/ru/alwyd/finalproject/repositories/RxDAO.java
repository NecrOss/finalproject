package ru.alwyd.finalproject.repositories;


import ru.alwyd.finalproject.entity.Rx;

import java.util.List;

public interface RxDAO {
	List<Rx> findByDoctorId(Long doctorId);
	
	List<Rx> findByUserId(Long userId);
	
	Rx save(Rx rx);
}
