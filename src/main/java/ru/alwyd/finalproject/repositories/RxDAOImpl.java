package ru.alwyd.finalproject.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.alwyd.finalproject.entity.Rx;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class RxDAOImpl implements RxDAO {

	private SessionFactory sessionFactory;
    
	@Autowired
	public RxDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<Rx> findByDoctorId(Long doctorId) {
		EntityManager em = sessionFactory.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Rx> rxByDoctorIdCriteria = cb.createQuery(Rx.class);
		Root<Rx> userRoot = rxByDoctorIdCriteria.from(Rx.class);
		rxByDoctorIdCriteria.select(userRoot);
		rxByDoctorIdCriteria.where(cb.equal(userRoot.get("doctorId"),  doctorId));
		return em.createQuery(rxByDoctorIdCriteria).getResultList();
	}

	@Override
	public List<Rx> findByUserId(Long userId) {
		EntityManager em = sessionFactory.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Rx> rxByUserIdCriteria = cb.createQuery(Rx.class);
		Root<Rx> userRoot = rxByUserIdCriteria.from(Rx.class);
		rxByUserIdCriteria.select(userRoot);
		rxByUserIdCriteria.where(cb.equal(userRoot.get("userId"),  userId));
		return em.createQuery(rxByUserIdCriteria).getResultList();
	}

	@Override
	public Rx save(Rx rx) {
		Session session = this.sessionFactory.openSession();
		session.save(rx);
		session.close();
		return rx;
	}
	

}
