package ru.alwyd.finalproject.repositories;


import ru.alwyd.finalproject.entity.Doctor;

import java.util.List;

public interface DoctorDAO {
	
	List<Doctor> findAll();
	
	List<Doctor> findBySpecialityCode(String code);
	
	int findAllCount();
	
	Doctor findByUserId(Long userId);

	Doctor save(Doctor doctor);
}
