package ru.alwyd.finalproject.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.alwyd.finalproject.entity.Doctor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class DoctorDAOImpl implements DoctorDAO {

	private SessionFactory sessionFactory;
	private EntityManagerFactory entityManagerFactory;
    
	@Autowired 
	public DoctorDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<Doctor> findBySpecialityCode(String code) {
		EntityManager em = sessionFactory.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Doctor> doctorBySpecialityCode  = cb.createQuery(Doctor.class);
		Root<Doctor> doctorRoot = doctorBySpecialityCode .from(Doctor.class);
		doctorBySpecialityCode .select(doctorRoot);
		doctorBySpecialityCode .where(cb.equal(cb.lower(doctorRoot.get("specialityCode")),  code.toLowerCase()));
		return em.createQuery(doctorBySpecialityCode ).getResultList();
	}
	
	@Override
	public Doctor findByUserId(Long userId) {
		EntityManager em = sessionFactory.createEntityManager();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Doctor> doctorByIdCriteria = cb.createQuery(Doctor.class);
		Root<Doctor> doctorRoot = doctorByIdCriteria.from(Doctor.class);
		doctorByIdCriteria.select(doctorRoot);
		doctorByIdCriteria.where(cb.equal(doctorRoot.get("userId"),  userId));
		return em.createQuery(doctorByIdCriteria).getSingleResult();
	}

	@Override
	public List<Doctor> findAll() {
		EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Doctor> doctorCriteria = cb.createQuery(Doctor.class);
        doctorCriteria.from(Doctor.class);
        return em.createQuery(doctorCriteria).getResultList();
	}

	@Override
	public int findAllCount() {
        EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> doctorCountCriteria = cb.createQuery(Long.class);
        doctorCountCriteria.select(cb.count(doctorCountCriteria.from(Doctor.class)));
        return em.createQuery(doctorCountCriteria).getSingleResult().intValue();
	}

	@Override
	public Doctor save(Doctor doctor) {
		Session session = this.sessionFactory.openSession();
		session.save(doctor);
		session.close();
		return doctor;
	}

}
