package ru.alwyd.finalproject.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.alwyd.finalproject.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	private SessionFactory sessionFactory;
    
	@Autowired 
	public UserDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

    @Override
	public List<User> findByEmail(String email) {
        EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userByEmailCriteria = cb.createQuery(User.class);
        Root<User> userRoot = userByEmailCriteria.from(User.class);
        userByEmailCriteria.select(userRoot);
        userByEmailCriteria.where(cb.equal(userRoot.get("email"),  email));
        return em.createQuery(userByEmailCriteria).getResultList();
	}

	@Override
	public List<User> findByEmailAndPassword(String email, String password) {
        EntityManager em = sessionFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userByEmailAndPasswordCriteria = cb.createQuery(User.class);
        Root<User> userRoot = userByEmailAndPasswordCriteria.from(User.class);
		userByEmailAndPasswordCriteria.select(userRoot);
		userByEmailAndPasswordCriteria.where(cb.equal(userRoot.get("email"),  email), cb.equal(userRoot.get("password"), password));
        return em.createQuery(userByEmailAndPasswordCriteria).getResultList();
	}

	@Override
	public User save(User user) {
		Session session = this.sessionFactory.openSession();
		session.save(user);
		session.close();
		return user;
	}
	
	@Override
	public User update(User user) {
		Session session = this.sessionFactory.openSession();
		session.beginTransaction();
		session.update(user);
		session.getTransaction().commit();
		session.close();
		return user;
	}
	
}
