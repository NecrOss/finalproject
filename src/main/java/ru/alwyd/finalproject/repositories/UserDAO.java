package ru.alwyd.finalproject.repositories;

import ru.alwyd.finalproject.entity.User;

import java.util.List;

public interface UserDAO  {
	
	User save(User user);
	
	List<User> findByEmail(String email);
	
	List<User> findByEmailAndPassword(String email, String password);
	
	User update(User user);
	
}
