package ru.alwyd.finalproject.helpers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alwyd.finalproject.entity.Doctor;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DoctorInfo {

    private String message;
    private List<Doctor> doctors;
    private int count;

    public DoctorInfo(String message, List<Doctor> doctors) {
        this.message = message;
        this.doctors = doctors;

    }
    public DoctorInfo(String message, int count) {
        this.message = message;
        this.count = count;
    }

}
