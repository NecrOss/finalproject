package ru.alwyd.finalproject.helpers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.alwyd.finalproject.entity.User;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExecutionStatus {

	private String code;
	private String message;
	private User user;
	
	public ExecutionStatus(String code, String message) {
		this.code = code;
		this.message = message;
	}

}
