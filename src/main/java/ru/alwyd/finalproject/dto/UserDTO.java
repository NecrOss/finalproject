package ru.alwyd.finalproject.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private int age;
    private int role;
    private int gender;
    private String speciality;
}
