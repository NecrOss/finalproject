package ru.alwyd.finalproject.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RxDTO {

    private String patientId;
    private String patientName;
    private String symptoms;
    private String medicine;

}
