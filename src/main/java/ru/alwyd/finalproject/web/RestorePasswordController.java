package ru.alwyd.finalproject.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.alwyd.finalproject.entity.User;
import ru.alwyd.finalproject.services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/account/restore/password")
public class RestorePasswordController {

    @Autowired
    private UserService userService;
    @Autowired
    private Map<String, User> passwordTokens;

    @RequestMapping(value = "/process/{email}/{token}", method = RequestMethod.GET)
    public ModelAndView processRestorePassword(@PathVariable String email, @PathVariable String token, HttpServletRequest request) {
        User user = userService.doesUserExist(email);

        if (!passwordTokens.containsKey(token)) {

        }

        return new ModelAndView("restorepassword");
    }

}
