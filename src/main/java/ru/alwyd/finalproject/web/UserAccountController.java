package ru.alwyd.finalproject.web;

import lombok.AllArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.alwyd.finalproject.dto.UserDTO;
import ru.alwyd.finalproject.entity.User;
import ru.alwyd.finalproject.helpers.ExecutionStatus;
import ru.alwyd.finalproject.services.DoctorService;
import ru.alwyd.finalproject.services.EmailService;
import ru.alwyd.finalproject.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping("/account/*")
public class UserAccountController {

    @Autowired
    private UserService userService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private Map<String, User> passwordTokens;

    @GetMapping(value = "/token")
    public Map<String, String> getToken(HttpSession httpSession) {
        return Collections.singletonMap("token", httpSession.getId());
    }

    @PostMapping(value="/signup")
    public ExecutionStatus processSignUp(@RequestBody UserDTO userDTO) {
        User user = userService.doesUserExist(userDTO.getEmail());

        if(user != null) {
            return new ExecutionStatus("USER_ACCOUNT_EXISTS", "User account with same email address exists. Please try again!", user);
        }

        user = new User();
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setAge(userDTO.getAge());
        user.setGender(userDTO.getGender());
        user.setRole(userDTO.getRole());
        User persistedUser = userService.save(user);

        if (user.getRole() == 1 && persistedUser != null) {
            doctorService.addDoctor(persistedUser, userDTO.getSpeciality());
        }

        return new ExecutionStatus("USER_ACCOUNT_CREATED", "User account successfully created", user);
    }

    @PostMapping(value="/user/update")
    public ExecutionStatus updateUser(@RequestBody UserDTO userDTO) {
        User user = userService.getByEmail(userDTO.getEmail());
        user.setId(user.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setAge(userDTO.getAge());
        user.setGender(userDTO.getGender());
        userService.update(user);
        return new ExecutionStatus("USER_ACCOUNT_UPDATED", "User account successfully updated");
    }

    @PostMapping(value="/forgotpassword/process", produces="application/json")
    public ExecutionStatus processForgotPassword(@RequestParam String email, @RequestParam String newPassword) {
        User user = userService.doesUserExist(email);

        RandomString rs = new RandomString(8);
        String token = rs.nextString();

        passwordTokens.put(token, user);

        emailService.sendSimpleMessage("alexeywyd@gmail.com", "Password restore url", "Please follow this link for restore your password:\n " +
                "http://localhost:8080/account/restorepassword/process/" + email + "/" + token);

        user.setPassword(newPassword);
        userService.update(user);

        return new ExecutionStatus("RESTORE_PASSWORD", "Password successfully changed", user);
    }

    @RequestMapping(value = "/restorepassword/process/{email}/{token}", method = RequestMethod.GET)
    public ModelAndView processRestorePassword(@PathVariable String email, @PathVariable String token, HttpServletRequest request) {
        User user = userService.doesUserExist(email);

        if (!passwordTokens.containsKey(token)) {

        }

        return new ModelAndView("restorepassword");
    }

}
