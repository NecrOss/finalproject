package ru.alwyd.finalproject.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.alwyd.finalproject.entity.Doctor;
import ru.alwyd.finalproject.helpers.DoctorInfo;
import ru.alwyd.finalproject.services.DoctorService;

import java.util.List;

@RestController
public class DoctorSearchController {

    @Autowired
    private DoctorService doctorService;

    @GetMapping(value="/doctor/count")
    public String getDoctorCount() {
        int count = doctorService.findCount();
        return String.valueOf(count);
    }

    @GetMapping(value="/doctor", produces="application/json")
    public DoctorInfo findAll() {
        List<Doctor> doctors = doctorService.findAll();

        if(doctors == null) {
            return new DoctorInfo("No Doctors found!", null);
        }

        return new DoctorInfo("Doctors found", doctors);
    }

    @GetMapping(value = "/doctor/{code}")
    public DoctorInfo getBySpecialityCode(@PathVariable String code) {
        List<Doctor> doctors = doctorService.findBySpeciality(code);

        if(doctors == null) {
            return new DoctorInfo("No Doctors found!", null);
        }

        return new DoctorInfo("Doctors found", doctors);
    }

}
