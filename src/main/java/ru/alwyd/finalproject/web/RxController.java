package ru.alwyd.finalproject.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.alwyd.finalproject.dto.RxDTO;
import ru.alwyd.finalproject.entity.Doctor;
import ru.alwyd.finalproject.entity.Rx;
import ru.alwyd.finalproject.entity.User;
import ru.alwyd.finalproject.services.DoctorService;
import ru.alwyd.finalproject.services.RxService;
import ru.alwyd.finalproject.services.UserService;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RxController {

    @Autowired
    private RxService rxService;

    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;

    @GetMapping(value="/rx", produces="application/json")
    public List<RxDTO> getRx() {
        List<Rx> rxList = null;
        User user = getUser();

        if(user.getRole() == 1) {
            rxList = rxService.findByDoctorId(user.getId());
        } else {
            rxList = rxService.findByPatientId(user.getId());
        }

        List<RxDTO> rxDTOList = new ArrayList<>();

        for(Rx rx: rxList) {
            RxDTO rxDTO = new RxDTO();
            rxDTO.setMedicine(rx.getMedicine());
            rxDTO.setSymptoms(rx.getSymptoms());
            rxDTO.setPatientName(rx.getUser().getFirstName());
            rxDTOList.add(rxDTO);
        }

        return rxDTOList;
    }

    @PostMapping(value="/rx/new", produces="application/json")
    public Rx createRx(@RequestBody RxDTO rxDTO) {
        Rx rx = new Rx();
        rx.setMedicine(rxDTO.getMedicine());
        rx.setSymptoms(rxDTO.getSymptoms());

        User patient = userService.getByEmail(rxDTO.getPatientId());
        rx.setUserId(patient.getId());

        Doctor doctor = doctorService.findByUserEmailAddress(getUserEmailAddress());
        rx.setDoctorId(doctor.getId());

        rx.setCreateTime(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));

        rxService.save(rx);

        return rx;
    }

    private User getUser() {
        String userEmailAddress = getUserEmailAddress();

        return userService.doesUserExist(userEmailAddress);
    }

    private String getUserEmailAddress() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
